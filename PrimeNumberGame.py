import random
import time
import tkinter as tk
import tkinter.ttk as ttk

# 'True' pie pirmkoda faila izveidos ģenerēto koku teksta veidā tā pārskatīšanai. Izmanto papildu resursus. 
# 'False' vienkārši neizveidos/neizvadīs neko failā un ietaupīs nedaudz resursus.
# Iekļauj visas virsotnes un lokus. 
# Šis ir tikai vizualizācijai un atkļūdošanai.
output_tree = False


###################################
### ----- Data Structures ----- ###
###################################

class Node:
    # Virsotne
    def __init__(self, id, available_numbers, current_num, p1, p2, level, weight):
        self.id = id                                # 0 - katras virsotnes unikāls id (numurēts sākot no 1, piem., X1, X42, u.t.t.)
        self.available_numbers = available_numbers  # 1 - pašreizējā ģenerēto skaitļu virkne (samazinās skaitā par 1 ar katru līmeni zemāk)
        self.current_num = current_num              # 2 - pašreizējais skaitlis ar ko veic darbības - saskaitīšanu vai atņemšanu
        self.p1 = p1                                # 3 - pašreizējais pirmā spēlētāja punktu skaits
        self.p2 = p2                                # 4 - pašreizējais otrā spēlētāja punktu skaits
        self.level = level                          # 5 - pašreizējais līmenis kokā
        self.weight = weight                        # 6 - virsotnes heiristiskais vērtējums (1, -1, vai 0)

class Tree:
    # Pats koks. Sastāv no virsotņu kopas un loku vārdīcas
    def __init__(self):
        self.set_of_nodes = [] # virsotņu kopa
        self.set_of_pointers = dict() # loku vārdīca (sākuma virsotne, [beigu virsotnes])

    def add_node(self, Node): # pievieno kokam jaunu virsotni
        self.set_of_nodes.append(Node)

    def add_pointer(self, start_node_id, end_node_id): # pievieno kokam jaunu loku
        self.set_of_pointers[start_node_id] = self.set_of_pointers.get(start_node_id, []) + [end_node_id]



#############################
### ----- Functions ----- ###
#############################
        
def isPrime(number): # Vai ir pirmskaitlis
    if number & 1 == 0: # vai pāra
        return False
    root = 3
    while root * root <= number:
        if number % root == 0:
            return False
        root += 2
    return True
        

def getNodes(level): # iegūst visas šī līmeņa virsotnes
    nodes = []
    for v in tree.set_of_nodes:
        if level == v.level:
            nodes.append(v)
    return nodes

def getChildrenID(node): # iegūst visas šīs virsotnes bērnu id
    child_ids = []
    for id, children in tree.set_of_pointers.items():
        if id == node.id:
            for child in children:
                child_ids.append(child)    
            return child_ids

def getNodeFromID(id): # iesgūst virsotni no tās id 
    for v in tree.set_of_nodes:
        if v.id == id:
            return v

def getChildrenWeights(child_ids): # iegūst visus vērtējumus no bērnu id
    weights = []
    for id in child_ids:
        child_node = getNodeFromID(id)
        weights.append(child_node.weight)
    return weights

# Minimaksa algoritms
def assignWeights(level): # piešķir visām pārējām virsotnēm heiristisko vērtējumu

    nodes = getNodes(level) # iegūst visas šī līmeņa virsotnes

    for v in nodes: # Katrai šī līmeņa virsotnei 
        child_ids = getChildrenID(v) # virsotnes bērnu id
        weights = getChildrenWeights(child_ids) # saraksts ar bērnu virsotņu heristiskajiem vērtējumiem (pirmajā funkcijas iterācijas reizē šie būs saknes virsotņu vērtējumi)

        # Izvēlas atbilstoši līmenim un pirmajam spēlētājam min vai max vērtējumu
        new_weight = None
        if maxStarts:
            if level % 2 == 0:          # min lvl
                new_weight = min(weights)
            else:                       # max lvl
                new_weight = max(weights)
        else:
            if level % 2 == 0:          # max lvl
                new_weight = max(weights)
            else:                       # min lvl
                new_weight = min(weights)

        # Heiristiskos vērtējumus piešķir atbilstošajām koka virsotnēm
        for vi in tree.set_of_nodes:
            if vi.id == v.id:
                vi.weight = new_weight

    # Iet vienu līmeni augstāk un sāk nākamo iterāciju, ja nākamais līmenis nav pirmais
    level -= 1
    if level != 1:
        assignWeights(level)



def check(turn_type, generated_nodes, current_node):
    if current_node[1]: # ja nav tukšs gadījuma skaitļu saraksts jeb, ja nav saknes virsotne
        
        global highscore # statistikai

        # Virsotņu id piešķiršanai
        global count
        new_id = 'X' + str(count)
        count += 1

        new_weight = current_node[6]

        new_p1 = current_node[3]
        new_p2 = current_node[4]

        new_available_numbers = current_node[1].copy() 

        number_next = new_available_numbers.pop(0) # iegūst gadījuma skaitli, ko izmantos, lai veiktu saskaitīšanu/atņemšanu ar pašreizējo skaitli jeb 'new_current_num'. Noņem šo skaitli no virknes.

        new_current_num = current_node[2] # iegūst pašreizējo skaitli
        
        if turn_type == '+':
            new_current_num += number_next
        elif turn_type == '-':
            new_current_num -= number_next

        # Piešķir +1 punktu gadījumā atbilstošajam spēlētājam, ja jaunais aprēķinātais skaitlis ir pirmskaitlis
        if (current_node[5] % 2) == 0: # pāra skaitļa līmenis: spēlētājs, kurš gāja otrais
            if isPrime(new_current_num):
                new_p2 += 1
            if highscore < new_p2:
                highscore = new_p2
        else: # nepāra skaitļa līmenis: spēlētājs, kurš gāja pirmais
            if isPrime(new_current_num): 
                new_p1 += 1
            if highscore < new_p1:
                highscore = new_p1

        # Heiristisko vērtējumu piešķiršana tikai saknes virsotnēm
        if not new_available_numbers: # ja saknes virsotne
            if maxStarts: # ja sāk AI
                if new_p1 > new_p2:
                    new_weight = 1
                elif new_p1 < new_p2:
                    new_weight = -1
                elif new_p1 == new_p2:
                    new_weight = 0
            else: # ja sāk spēlētājs/cilvēks
                if new_p1 > new_p2:
                    new_weight = -1
                elif new_p1 < new_p2:
                    new_weight = 1
                elif new_p1 == new_p2:
                    new_weight = 0

        new_current_num = abs(new_current_num)

        new_level = current_node[5] + 1
        new_node = Node(new_id, new_available_numbers, new_current_num, new_p1, new_p2, new_level, new_weight)

        # Pārbaude, vai virsotne jau ir kokā
        already_in_tree = False
        i = 0
        while (not already_in_tree) and (i <= len(tree.set_of_nodes) - 1):
            if (tree.set_of_nodes[i].available_numbers == new_node.available_numbers) and \
                    (tree.set_of_nodes[i].current_num == new_node.current_num) and \
                    (tree.set_of_nodes[i].p1 == new_node.p1) and \
                    (tree.set_of_nodes[i].p2 == new_node.p2) and \
                    (tree.set_of_nodes[i].level == new_node.level) and \
                    (tree.set_of_nodes[i].weight == new_node.weight):
                already_in_tree = True
            else:
                i += 1

        if not already_in_tree: # ja nav kokā
            global notMathes
            notMathes += 1
            tree.add_node(new_node) # pievieno kokam jauno virsotni
            generated_nodes.append([new_id, new_available_numbers, new_current_num, new_p1, new_p2, new_level, new_weight]) # pievieno ģenerēto virsotņu kopai jauno virsotni
            tree.add_pointer(current_node[0], new_id) # loku vārdnīcai pievieno loku
        else: # ja jau ir kokā
            global matches
            matches += 1
            count -= 1
            tree.add_pointer(current_node[0], tree.set_of_nodes[i].id) # loku vārdnīcai pievieno loku no šīs virsotnes uz jau eksistējošo


def getNextNodes(current_node_id): # Iegūst šīs virsotnes nākamās visrotnes (bērnus) no loku vārdnīcas
    for id, nodes in tree.set_of_pointers.items(): 
        if id == current_node_id:
            return nodes

def getNextBestMove(current_node_id): # Iegūst labāko gājienu
    
    next_nodes = getNextNodes(current_node_id) # šīs virsotnes nākamās visrotnes

    nextMove = None
    max_weight = -2

    first_score = None
    second_score = None

    first_weight = None
    second_weight = None

    first_id = None
    second_id = None

    if next_nodes: # ja nav tukšs
        for v in tree.set_of_nodes:
            for n in next_nodes:
                if v.id == n:

                    # Ja abu nākamo virsotņu vērtējumi ir vienādi, bet tomēr vienā ir lielāks punktu daudzums, 
                    # to izvēlēsies kā nākamo (iepriekš izvēlējās pirmo pēc kārtas)
                    if not nextMove: # ja pirmo reizi, piešķir šīs vērtības
                        nextMove = v.id
                        first_weight = v.weight
                        first_id = v.id
                        max_weight = v.weight
                        if maxStarts:
                            first_score = v.p1
                        else:
                            first_score = v.p2
                    else:
                        if v.weight > max_weight: # otrajai virsotne, ja vērtējums lielāks, to izvēlas
                            return v.id

                        second_weight = v.weight
                        second_id = v.id
                        if maxStarts:
                            second_score = v.p1
                        else:
                            second_score = v.p2
                        
                        if first_weight == second_weight:
                            if first_score > second_score:
                                nextMove = first_id
                            elif first_score < second_score:
                                nextMove = second_id
         
        return nextMove
    



## GUI functions ##

def getButtonPressed(answer):
    button_answer.set(answer)

def getButtonEntry():
    entry_answer.set(entry.get())

def clear_window():
    for element in window.winfo_children():
        element.destroy()
    button_is_pressed.set(True)



###############################
### ----- GAME / Main ----- ###
###############################

window = tk.Tk()
window.title("Prime Number Game")
window.geometry('800x700')

keep_playing = True
while keep_playing: # Visas spēles garumā. Atkārtos, ja spēlētājs vēlēsies spēlēt vēlreiz

    upside_frame = ttk.Frame()
    question_label = ttk.Label(master=upside_frame, text="Who'll go first?", font=('',14,'bold'))
    question_label.pack(pady=30)


    button_frame = ttk.Frame()
    me_button = ttk.Button(master=button_frame, text="Me", 
                        command=lambda: getButtonPressed("Me"))
    me_button.pack(side=tk.LEFT, padx=25, pady=10)
    ai_button = ttk.Button(master=button_frame, text="AI", 
                        command=lambda: getButtonPressed("AI"))
    ai_button.pack(side=tk.RIGHT, padx=25, pady=10)

    upside_frame.pack()
    button_frame.pack()

    button_answer = tk.StringVar()
    window.wait_variable(button_answer)

    button_frame.pack_forget()


    # whoFirst = input("Who starts first: ")
    maxStarts = False
    if button_answer.get() == "AI":
        maxStarts = True

    # Cik skaitļus?
    button_frame.pack_forget()
    upside_frame.pack_forget()
    upside_frame = ttk.Frame()
    ttk.Label(master=upside_frame, text="How many numbers to generate? [3 to 14]").pack()
    ttk.Label(master=upside_frame, text="(more than 12 can take a few minutes)", font=('', 8, 'italic')).pack()
    upside_frame.pack(pady=30)

    valid_number = False
    while not valid_number:
        downside_frame = ttk.Frame()
        entry = ttk.Entry(master=downside_frame, width=5)
        entry.pack()
        downside_frame.pack(pady=20)

        button = ttk.Button(window, text="Save", command=getButtonEntry)
        button.pack(pady=10)

        entry_answer = tk.StringVar()
        window.wait_variable(entry_answer)
        # print(entry_answer.get())
        answer = entry_answer.get()

        if answer.isdigit():
            num = int(answer)
            if 3 <= num <= 14:
                valid_number = True

        downside_frame.pack_forget()
        button.pack_forget()


    upside_frame.pack_forget()

    noLevels = num


    upside_frame = ttk.Frame()
    ttk.Label(master=upside_frame, text="Generate numbers from 1 to (default 1000):").pack(pady=30)
    upside_frame.pack()

    valid_number = False
    while not valid_number:
        downside_frame = ttk.Frame()
        entry = ttk.Entry(master=downside_frame, width=7)
        entry.pack()
        entry.insert(0, '1000')
        downside_frame.pack(pady=20)

        button = ttk.Button(window, text="Save", command=getButtonEntry)
        button.pack(pady=10)

        entry_answer = tk.StringVar()
        window.wait_variable(entry_answer)
        answer = entry_answer.get()

        if answer.isdigit():
            num_range = int(answer)
            if 1 < num_range:
                valid_number = True

        downside_frame.pack_forget()
        button.pack_forget()

    upside_frame.pack_forget()



    # 10 skaitļi, no kuriem jāizvēlas viens katrā gājienā
    generated_numbers = []
    for i in range(noLevels):
        generated_numbers.append(random.randint(1, num_range))

    # "Start_number"/main skaitlis
    # start_number = 0
    start_number = random.randint(1, num_range)


    upside_frame = ttk.Frame()
    ttk.Label(master=upside_frame, text=f'The {num} generated numbers are:').pack(pady=15)
    upside_frame.pack()

    gen_nums = ttk.Frame()
    ttk.Label(master=gen_nums, text=f'{generated_numbers}', font=('', 12)).pack()
    gen_nums.pack()

    main_number_frame = ttk.Frame()
    ttk.Label(master=main_number_frame, text=f'The main/starting number is:   {start_number}').pack(pady=45)
    main_number_frame.pack()


    # Start
    button_is_pressed = tk.BooleanVar(value=False)

    next_button = ttk.Button(window, text="Start Game!", command=clear_window)
    next_button.pack(pady=10)

    window.wait_variable(button_is_pressed)

    # 'Please Wait...' kamēr ģenerē koku
    please_wait = ttk.Label(window, text='Please Wait...', font=('',25))
    please_wait.pack(pady=100)

    window.update_idletasks()


    tree = Tree()
    generated_nodes = []

    # Sākuma virsotne
    tree.add_node(Node('X1', generated_numbers, start_number, 0, 0, 1, None))
    generated_nodes.append(['X1', generated_numbers, start_number, 0, 0, 1, None])

    # Virsotņu skaitītājs
    count = 2

    notMathes = 0
    matches = 0


    highscore = 0

    start_time = time.time() # Sākuma laiks koka ģenerēšanai

    # Apskata visas ģenerētās virsotnes
    while len(generated_nodes) > 0:
        current_node = generated_nodes[0]

        check('+', generated_nodes, current_node) # Gadījums, kad izvēlas saskaitīšanu
        check('-', generated_nodes, current_node) # Gadījums, kad izvēlas atņemšanu

        generated_nodes.pop(0)

    assignWeights(noLevels) # Piešķir katrai virsotnei heiristisko vērtējumu


    tree_gen_time = time.time() - start_time # Koka ģenerācijas laiks


    aiWins = 0
    humanWins = 0
    tie = 0

    # Viss ģenerētais spēlēs koks vieglākai pārskatīšanai
    if output_tree:
        file1 = open('game_tree_debug.txt', 'w')

    smallest_num = float('inf')
    for x in tree.set_of_nodes: # Cikls statistikai vai virsotņu/loku rakstīšanai failā (ja iespējots 'output_tree')
        if output_tree:
            file1.writelines(f'{x.id} {x.available_numbers} {x.current_num} {x.p1} {x.p2} {x.level} {x.weight}\n')

        if not x.available_numbers:
            if x.weight == 1:
                aiWins += 1
            elif x.weight == -1:
                humanWins += 1
            else:
                tie += 1
        
        if x.current_num < smallest_num:
            smallest_num = x.current_num
    
    if output_tree:
        for x, y in tree.set_of_pointers.items():
            file1.writelines(f'{x} {y}\n') 

        file1.close()


    # Beidzas 'Please Wait...'
    clear_window()


    # maxTurn atbilst pašreizējam AI gājienam
    # maxStarts atbilst spēlei, kurā AI veic pirmo gājienu
    maxTurn = False
    turnString = "Your turn!"
    if maxStarts:
        maxTurn = True
        turnString = "AI's turn!"

    current_lvl = 1
    current = 'X1' 
    main_number = start_number
    while current_lvl <= noLevels: # viss vienas spēles garums
        current_node = getNodeFromID(current)

        # Pašreizējo punktu daudzums
        if maxStarts:
            ai_points = current_node.p1
            human_points = current_node.p2
        else:
            human_points = current_node.p1
            ai_points = current_node.p2

        # Uzraksts, kura gājiens tagad ir.
        turn_frame = ttk.Frame()
        ttk.Label(master=turn_frame, text=f'{turnString}', font=('', 25, 'bold')).pack()
        ttk.Label(master=turn_frame, text=f'Level {current_lvl}').pack()
        ttk.Label(master=turn_frame, text=f'(You)   {human_points}   vs   {ai_points}   (AI)').pack()
        turn_frame.pack(pady=40)
        # 'With X number do:'
        numbers_frame = ttk.Frame()
        ttk.Label(master=numbers_frame, text=f'{current_node.available_numbers}').pack()
        ttk.Label(master=numbers_frame, text=f'With {main_number} do:', font=('', 14, 'bold', 'italic')).pack(pady=7)
        numbers_frame.pack(pady=10)

        if maxTurn: # AI gājiens
            nextMove = getNextBestMove(current) # iegūst nākamo labāko gājienu
            
            choise_frame = ttk.Frame()
            ttk.Label(master=choise_frame, 
                text=f'+{current_node.available_numbers[0]}  or  -{current_node.available_numbers[0]}').pack()
            ttk.Label(master=choise_frame, text='=', font=('', 12, 'italic')).pack()
            ttk.Label(master=choise_frame, 
                text=f'{current_node.available_numbers[0]+main_number}  or  {abs(current_node.available_numbers[0]-main_number)}', font=('', 12,'bold')).pack()
            choise_frame.pack()

            choise_result_frame = ttk.Frame()
            ttk.Label(master=choise_result_frame,
                text=f'AI chose {getNodeFromID(nextMove).current_num}', font=('', 12, 'bold', 'underline')).pack()
            
            # Sagatavo cilvēka gājienam
            maxTurn = False
            turnString = "Your turn!"
            current = nextMove
            main_number = getNodeFromID(current).current_num
        else: # Spēlētāja/cilvēka gājiens
            next_available_nodes = getNextNodes(current) # iegūst nākamās pieejamās virsotnes/gājienus

            button_frame = ttk.Frame()
            ttk.Button(master=button_frame, 
                text=f"+{current_node.available_numbers[0]}", 
                command=lambda: getButtonPressed('+')).pack(side=tk.LEFT, padx=25, pady=10)
            ttk.Button(master=button_frame, text=f"-{current_node.available_numbers[0]}", 
                        command=lambda: getButtonPressed("-")).pack(side=tk.RIGHT, padx=25, pady=10)
            button_frame.pack()

            choise_frame = ttk.Frame()
            ttk.Label(master=choise_frame, text='=', font=('', 12, 'italic')).pack()
            ttk.Label(master=choise_frame, 
                text=f'{current_node.available_numbers[0]+main_number}        or        {abs(current_node.available_numbers[0]-main_number)}', font=('', 12,'bold')).pack()
            choise_frame.pack()

            button_answer = tk.StringVar()
            window.wait_variable(button_answer)
            button_frame.pack_forget()
            
            # Iegūst spēlētāja izvēli
            if button_answer.get() == '+':
                nextMove = next_available_nodes[0]
            else:
                nextMove = next_available_nodes[1]

            choise_result_frame = ttk.Frame()
            ttk.Label(master=choise_result_frame,
                text=f'You chose {getNodeFromID(nextMove).current_num}', font=('', 12, 'bold', 'underline')).pack()
            
            # Sagatavo AI gājienam
            maxTurn = True
            turnString = "AI's turn!"
            current = nextMove
            main_number = getNodeFromID(current).current_num

        current_lvl += 1 # +1 līmenis nākamai iterācijai

        # Ir vai nav pirmskaitlis
        if isPrime(getNodeFromID(nextMove).current_num):
            ttk.Label(master=choise_result_frame,
            text=f"...IT IS a prime! +1p").pack()
        else:
            ttk.Label(master=choise_result_frame,
            text=f"...it is NOT a prime. +0p").pack()

        choise_result_frame.pack(pady=10)

        # Poga, kas nodod gājienu spēlētājam
        button_is_pressed = tk.BooleanVar(value=False)
        next_button = ttk.Button(window, text="Continue", command=clear_window)
        next_button.pack(pady=30)
        window.wait_variable(button_is_pressed)

        # clear_window()


    # Rezultātu logs #

    final_node = getNodeFromID(current)

    # Uzvarētāja noteikšana
    if maxStarts:
        if final_node.p1 > final_node.p2:
            whoWon = 'AI'
        elif final_node.p1 == final_node.p2:
            whoWon = 'Draw'
        else:
            whoWon = 'You'
        ai_points = final_node.p1
        human_points = final_node.p2
    else:
        if final_node.p1 > final_node.p2:
            whoWon = 'You'
        elif final_node.p1 == final_node.p2:
            whoWon = 'Draw'
        else:
            whoWon = 'AI'
        human_points = final_node.p1
        ai_points = final_node.p2


    # window.update_idletasks()

    upside_frame = ttk.Frame()
    if whoWon != 'Draw':
        ttk.Label(master=upside_frame, text=f'{whoWon} won!', font=('', 25, 'bold')).pack()
    else:
        ttk.Label(master=upside_frame, text="It's a Draw!", font=('', 25, 'bold')).pack()
    ttk.Label(master=upside_frame, text=f'(You)   {human_points}   vs   {ai_points}   (AI)').pack()
    upside_frame.pack(pady=50)

    total_outcomes = aiWins + humanWins + tie

    stats_frame = ttk.Frame()
    ttk.Label(master=stats_frame, text='Game Stats', font=('', 14)).pack()
    ttk.Label(master=stats_frame, text=f'Game tree generation time: {round(tree_gen_time, 5)} s').pack()
    ttk.Label(master=stats_frame, text=f'Highest possible points: {highscore}p').pack()
    ttk.Label(master=stats_frame, text=f'Your possible wins: {humanWins} ({round(humanWins/total_outcomes*100,2)}%)').pack()
    ttk.Label(master=stats_frame, text=f'AI possible wins: {aiWins} ({round(aiWins/total_outcomes*100,2)}%)').pack()
    ttk.Label(master=stats_frame, text=f'Possible draws: {tie} ({round(tie/total_outcomes*100,2)}%)').pack()
    stats_frame.pack(pady=15)

    # Izvēle par atkārtotu spēlēšanu
    button_frame = ttk.Frame()
    ttk.Button(master=button_frame, 
        text="Play Again", 
        command=lambda: getButtonPressed('Play Again')).pack(side=tk.LEFT, padx=25, pady=10)
    ttk.Button(master=button_frame, text="Exit", 
        command=lambda: getButtonPressed('Exit')).pack(side=tk.RIGHT, padx=25, pady=10)

    button_frame.pack()

    button_answer = tk.StringVar()
    window.wait_variable(button_answer)
    button_frame.pack_forget()

    button_answer = button_answer.get()
    if button_answer == 'Exit': 
        keep_playing = False



    # print("\n\n\n--- %s seconds ---" % (time.time() - start_time))

    # print("total count: ", count)
    # print("Not matches: ", notMathes)
    # print("Matches: ", matches)
    # print("Possible highest score: ", highscore)
    # print("AI wins: ",aiWins)
    # print("Human wins: ",humanWins)
    # print("Draws: ", tie)
    # print("Smallest number: ",smallest_num)

    clear_window()

